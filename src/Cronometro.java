
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.event.ActionListener;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author joao_elias
 */
public class Cronometro implements ActionListener{
    
    JFrame janela = new JFrame();
    JButton btIniciar = new JButton("►");
    JButton btResetar = new JButton("■");
    JLabel lbTempo = new JLabel();
    int tmpDecorrido = 0;
    int segundos = 0;
    int minutos = 0;
    int horas = 0;
    boolean iniciou = false;
    String segundos_String = String.format("%02d", segundos);
    String minutos_String = String.format("%02d", minutos);
    String horas_String = String.format("%02d", horas);
    
    Timer timer = new Timer(1000, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            tmpDecorrido+=1000;
            horas = (tmpDecorrido/3600000);
            minutos = (tmpDecorrido/60000) % 60;
            segundos = (tmpDecorrido/1000) % 60;
            segundos_String = String.format("%02d", segundos);
            minutos_String = String.format("%02d", minutos);
            horas_String = String.format("%02d", horas);
            lbTempo.setText(horas_String + " : " + minutos_String + " : " + segundos_String);
        }
    });
    
    Cronometro(){
        
        lbTempo.setText(horas_String + " : " + minutos_String + " : " + segundos_String);
        lbTempo.setBounds(100,100,200,100);
        lbTempo.setFont(new Font("Segoe UI", Font.PLAIN, 35));
        lbTempo.setBorder(BorderFactory.createBevelBorder(1));
        lbTempo.setOpaque(true);
        lbTempo.setHorizontalAlignment(JTextField.CENTER);
        
        btIniciar.setBounds(100,200,100,50);
        btIniciar.setFont(new Font("Segoe UI", Font.PLAIN, 20));
        btIniciar.setFocusable(false);
        btIniciar.addActionListener(this);
        
        btResetar.setBounds(200,200,100,50);
        btResetar.setFont(new Font("Segoe UI", Font.PLAIN, 20));
        btResetar.setFocusable(false);
        btResetar.addActionListener(this);
        
        janela.add(btIniciar);
        janela.add(btResetar);
        janela.add(lbTempo);
        janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        janela.setSize(420,420);
        janela.setLayout(null);
        janela.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==btIniciar){
            iniciar();
            if(iniciou == false){
                iniciou = true;
                btIniciar.setText("▋▋");
                iniciar();
            }
            else{
                iniciou = false;
                btIniciar.setText("►");
                parar();
            }
        }
        
        if(e.getSource() == btResetar){
            iniciou = false;
            btIniciar.setText("►");
            resetar();
        }
    }
    
    void iniciar(){
        timer.start();
    }
    
    void parar(){
        timer.stop();
    }
    
    void resetar(){
        timer.stop();
        tmpDecorrido = 0;
        segundos = 0;
        minutos = 0;
        horas = 0;
        segundos_String = String.format("%02d", segundos);
        minutos_String = String.format("%02d", minutos);
        horas_String = String.format("%02d", horas);
        lbTempo.setText(horas_String + " : " + minutos_String + " : " + segundos_String);
    }
}
